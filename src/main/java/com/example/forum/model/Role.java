package com.example.forum.model;

import java.util.Set;

public enum Role {
    ROLE_SUPER_ADMIN,
    ROLE_ADMIN,
    ROLE_MODERATOR,
    ROLE_USER
}
