package com.example.forum.repository;

import com.example.forum.model.Enterprise;
import com.example.forum.model.Role;
import com.example.forum.model.Status;
import com.example.forum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByUsername(String username);

    Optional<User> findByEnterpriseAndId(Enterprise enterprise, Integer id);

    Optional<User> findByEmail(String email);

    List<User> findAllByEnterprise(Enterprise enterprise);

    User findByRole(Role role);

    void deleteByEnterpriseAndId(Enterprise enterprise, Integer id);

    @Modifying
    @Query("update User u set u.status = ?1 where u.id = ?2")
    void setUserStatusById(Status status, Integer userId);

    @Modifying
    @Query("update User u set u.status = ?1 where u.enterprise.id = ?2 and u.id = ?3")
    void setUserStatusByEnterpriseAndId(Status status, Integer enterpriseId, Integer userId);

    @Modifying
    @Query("update User u set u.role = ?1 where u.id = ?2")
    void setUserRoleById(Role role, Integer userId);

    @Modifying
    @Query("update User u set u.role = ?1 where u.enterprise.id = ?2 and u.id = ?3")
    void setUserRoleByEnterpriseAndId(Role role, Integer enterpriseId, Integer userId);

    @Query("SELECT u FROM User u WHERE u.username LIKE %:usernameOrEmail% OR u.email LIKE %:usernameOrEmail% ")
    List<User> findAllByUsernameOrEmail(String usernameOrEmail);

    @Query("SELECT u FROM User u WHERE u.enterprise.id = (:enterpriseId) and u.username LIKE %:usernameOrEmail% OR u.email LIKE %:usernameOrEmail% ")
    List<User> findAllByEnterpriseAndUsernameOrEmail(@Param("usernameOrEmail") String usernameOrEmail, @Param("enterpriseId") Integer enterpriseId);

}
