package com.example.forum.repository;

import com.example.forum.model.Category;
import com.example.forum.model.Enterprise;
import com.example.forum.model.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Integer> {

    @Query("SELECT t FROM Topic t LEFT JOIN t.comments WHERE t.enterprise.id = (:enterpriseId) AND t.id = (:id)")
    Topic findByIdAndEnterpriseIdAndFetchComments(@Param("id") Integer id, @Param("enterpriseId") Integer enterpriseId);

    @Query("SELECT t FROM Topic t LEFT JOIN t.comments WHERE t.id = (:id)")
    Topic findByIdAndFetchComments(@Param("id") Integer id);

    Page<Topic> findAllByEnterpriseIdAndCategory(Pageable pageable, Integer enterpriseId, Category category);

    Page<Topic> findAllByCategory(Pageable pageable, Category category);

    Page<Topic> findAllByEnterpriseIdAndHeaderContainingIgnoreCase(Pageable pageable, Integer enterpriseId, String key);

    Page<Topic> findAllByHeaderContainingIgnoreCase(Pageable pageable, String key);

    Page<Topic> findAllByEnterprise(Pageable pageable, Enterprise enterprise);

    void deleteByEnterpriseAndId(Enterprise enterprise, Integer id);

}
