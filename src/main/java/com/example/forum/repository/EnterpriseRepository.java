package com.example.forum.repository;

import com.example.forum.model.Enterprise;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnterpriseRepository extends CrudRepository<Enterprise, Integer>  {
    Enterprise findByName(String name);
    List<Enterprise> findByNameContainingIgnoreCase(@Param("name") String name);

    List<Enterprise> findAllByIdIsNot(Integer id);
}
