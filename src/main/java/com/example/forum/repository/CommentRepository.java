package com.example.forum.repository;

import com.example.forum.model.Comment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
    Optional<Comment> findById(Integer id);

    @Modifying
    @Query("DELETE FROM Comment c WHERE c.topic.enterprise.id = (:enterpriseId) AND c.id = (:id)")
    void deleteByTopicEnterpriseIdAndId(@Param("id") Integer id, @Param("enterpriseId") Integer enterpriseId);
}
