package com.example.forum.controller;

import com.example.forum.model.Enterprise;
import com.example.forum.service.EnterpriseService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
@Slf4j
public class EnterpriseController {
    EnterpriseService enterpriseService;

    @GetMapping("/enterprises")
    public String getAll(Model model, @RequestParam(required = false) String searchValue) {
        log.info("searchValue={}", searchValue);
        if (searchValue != null && !searchValue.isEmpty()) {
            model.addAttribute("enterprises", enterpriseService.findAllByName(searchValue));
        } else {
            model.addAttribute("enterprises", enterpriseService.findAll(true));
        }
        model.addAttribute("enterpriseToSave", new Enterprise());
        return "enterprise/enterprise-table";
    }

    @GetMapping("/enterprises/add")
    public String showAddEnterpriseForm(@ModelAttribute("enterpriseToSave") Enterprise enterpriseToSave,
                                        BindingResult result,
                                        Model model) {
        model.addAttribute("showAddForm", true);
        model.addAttribute("enterprises", enterpriseService.findAll(true));

        if (result.hasErrors()) {
            model.addAttribute("enterpriseToSave", enterpriseToSave);
            model.addAttribute("errors", result);
        } else {
            model.addAttribute("enterpriseToSave", new Enterprise());
        }

        return "enterprise/enterprise-table";
    }

    @PostMapping("/enterprises/add")
    public String addEnterprise(@Valid @ModelAttribute("enterpriseToSave") Enterprise enterpriseToSave,
                                BindingResult result,
                                Model model) {
        if (result.hasErrors()) {
            model.addAttribute("showAddForm", true);
            model.addAttribute("enterpriseToSave", enterpriseToSave);
            model.addAttribute("enterprises", enterpriseService.findAll(true));
            model.addAttribute("errors", result);
            return "enterprise/enterprise-table";
        }
        enterpriseService.save(enterpriseToSave);
        model.addAttribute("enterpriseToSave", new Enterprise());
        return "redirect:/enterprises";
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public String handleDataIntegrityViolationException(DataIntegrityViolationException ex, RedirectAttributes redirectAttributes) {
        return "redirect:/error";
    }

    @DeleteMapping("/enterprises/{id}")
    public String deleteEnterprise(@PathVariable Integer id) {
        Enterprise enterprise = enterpriseService.findById(id);
        if (enterprise != null) {
            enterpriseService.delete(enterprise);
        }
        return "redirect:/enterprises";
    }
}
