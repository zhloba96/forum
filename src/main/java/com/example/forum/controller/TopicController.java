package com.example.forum.controller;


import com.example.forum.model.TopicMark;
import com.example.forum.model.dto.CommentDTO;
import com.example.forum.model.dto.TopicDTO;
import com.example.forum.security.jwt.LoginUser;
import com.example.forum.service.CategoryService;
import com.example.forum.service.CommentService;
import com.example.forum.service.TopicMarkService;
import com.example.forum.service.TopicService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
@Slf4j
public class TopicController {

    private static final String DEFAULT_SORT_PARAM = "creationDate";
    private static final int FIRST_PAGE_NUMBER = 1;
    private final TopicService topicService;
    private final CategoryService categoryService;
    private final CommentService commentService;
    private final TopicMarkService topicMarkService;

    @GetMapping
    public String getAllPageWithPagination(@RequestParam(name = "sortParam", defaultValue = DEFAULT_SORT_PARAM) String sortParam,
                                           @RequestParam(required = false, name = "category") Integer categoryId,
                                           @LoginUser Integer userId,
                                           Model model) {
        model.addAttribute(sortParam);
        if (categoryId != null) {
            model.addAttribute("categoryId", categoryId);
            return getOnePageWithCategoryFilter(sortParam, categoryId, userId, FIRST_PAGE_NUMBER, model);
        }
        return getOnePage(sortParam, userId, FIRST_PAGE_NUMBER, model);
    }

    @GetMapping("/page/{pageNumber}")
    public String getOnePage(@RequestParam(name = "sortParam", defaultValue = DEFAULT_SORT_PARAM) String sortParam,
                             @LoginUser Integer userId,
                             @PathVariable("pageNumber") int currentPage,
                             Model model) {
        Page<TopicDTO> page = topicService.findPage(currentPage, sortParam);
        int totalPages = page.getTotalPages();
        long totalItems = page.getTotalElements();
        List<TopicDTO> topics = page.getContent();
        topics.forEach(t -> topicService.actualizeDataDTO(t, userId));

        model.addAttribute("topics", topics);
        model.addAttribute("sortParam", sortParam);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("firstPage", FIRST_PAGE_NUMBER);
        return "index";
    }

    @GetMapping("/category/{pageNumber}")
    public String getOnePageWithCategoryFilter(@RequestParam(name = "sortParam", defaultValue = DEFAULT_SORT_PARAM) String sortParam,
                                               @RequestParam Integer categoryId,
                                               @LoginUser Integer userId,
                                               @PathVariable("pageNumber") int currentPage,
                                               Model model) {
        Page<TopicDTO> page = topicService.findPage(currentPage, sortParam, categoryId);
        int totalPages = page.getTotalPages();
        long totalItems = page.getTotalElements();
        List<TopicDTO> topics = page.getContent();
        topics.forEach(t -> topicService.actualizeDataDTO(t, userId));

        model.addAttribute("topics", topics);
        model.addAttribute("categoryId", categoryId);
        model.addAttribute("sortParam", sortParam);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("firstPage", FIRST_PAGE_NUMBER);
        return "index";
    }

    @GetMapping("/search")
    public String getAllPageByHeader(@RequestParam(name = "sortParam", defaultValue = DEFAULT_SORT_PARAM) String sortParam,
                                     @RequestParam(name = "key") String key,
                                     @LoginUser Integer userId,
                                     Model model) {
        model.addAttribute(sortParam);
        model.addAttribute("key", key);
        return getOnePageByHeader(sortParam, key, userId, FIRST_PAGE_NUMBER, model);
    }

    @GetMapping("/search/{pageNumber}")
    public String getOnePageByHeader(@RequestParam(name = "sortParam", defaultValue = DEFAULT_SORT_PARAM) String sortParam,
                                     @RequestParam(name = "key") String key,
                                     @LoginUser Integer userId,
                                     @PathVariable("pageNumber") int currentPage,
                                     Model model) {
        Page<TopicDTO> page = topicService.findPage(currentPage, sortParam, key);
        int totalPages = page.getTotalPages();
        long totalItems = page.getTotalElements();
        List<TopicDTO> topics = page.getContent();
        topics.forEach(t -> topicService.actualizeDataDTO(t, userId));

        model.addAttribute("topics", topics);
        model.addAttribute("key", key);
        model.addAttribute("sortParam", sortParam);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("firstPage", FIRST_PAGE_NUMBER);
        return "index";
    }


    @GetMapping("/topic/add")
    public String addTopic(Model model) {
        model.addAttribute("topic", new TopicDTO());
        model.addAttribute("categories", categoryService.findAll());
        return "topic/topic-add";
    }

    @PostMapping("/topic/add")
    public String addTopic(@Valid @ModelAttribute("topic") TopicDTO topic,
                           BindingResult result,
                           @LoginUser Integer userId,
                           Model model) {
        if (result.hasErrors()) {
            model.addAttribute("categories", categoryService.findAll());
            return "topic/topic-add";
        }

        if (topicService.save(topic, userId)) {
            return "redirect:/";
        } else {
            return "topic/topic-add";
        }
    }

    @GetMapping("/topic/{id}")
    public String getTopic(@PathVariable("id") int topicId,
                           @LoginUser Integer userId,
                           Model model) {
        model.addAttribute("topic", topicService.actualizeDataDTO(
                topicService.findByIdEagerly(topicId), userId));
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("mark", new TopicMark());
        return "topic/topic";
    }

    @PostMapping("/topic/{id}/mark-set")
    public String setMark(@RequestParam(name = "topicId") Integer topicId,
                          @RequestParam(name = "mark") Boolean mark,
                          @LoginUser Integer userId,
                          Model model) {
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("mark", new TopicMark());
        topicMarkService.saveMark(mark, userId, topicId);
        model.addAttribute("topic", topicService.actualizeDataDTO(
                topicService.findByIdEagerly(topicId), userId));
        return "topic/topic";
    }

    @PostMapping("/topic/comment")
    public String addComment(@Valid @ModelAttribute("comment") CommentDTO commentDTO,
                             BindingResult result,
                             @LoginUser Integer userId,
                             Model model) {
        if (result.hasErrors()) {
            model.addAttribute("commentDTO", commentDTO);
            model.addAttribute("mark", new TopicMark());
            model.addAttribute("errors", result);
            model.addAttribute("topic", topicService.actualizeDataDTO(
                    topicService.findByIdEagerly(commentDTO.getTopicId()), userId));
            return "topic/topic";
        }

        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("mark", new TopicMark());

        commentService.save(commentDTO, userId);
        model.addAttribute("topic", topicService.actualizeDataDTO(
                topicService.findByIdEagerly(commentDTO.getTopicId()), userId));

        return "topic/topic";
    }

    @DeleteMapping("/topic/{topicId}/comment/{commentId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MODERATOR') or hasRole('ROLE_SUPER_ADMIN')")
    public String deleteComment(@PathVariable(name = "commentId") Integer commentId,
                                @PathVariable(name = "topicId") Integer topicId) {
        commentService.deleteById(commentId);


        return "redirect:/topic/"+topicId;
    }

    @DeleteMapping("/topic/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MODERATOR') or hasRole('ROLE_SUPER_ADMIN')")
    public String deleteTopic(@PathVariable Integer id) {

        topicService.deleteById(id);
        return "redirect:/";
    }
}
