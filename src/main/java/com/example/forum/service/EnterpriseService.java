package com.example.forum.service;

import com.example.forum.model.Enterprise;
import com.example.forum.model.Role;
import com.example.forum.repository.EnterpriseRepository;
import com.example.forum.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
@Transactional
public class EnterpriseService {
    EnterpriseRepository enterpriseRepository;
    UserRepository userRepository;

    public List<Enterprise> findAllByName(String name) {
        return enterpriseRepository.findByNameContainingIgnoreCase(name);
    }

    public Enterprise findByName(String name) {
        return enterpriseRepository.findByName(name);
    }

    public List<Enterprise> findAll(Boolean force) {
        if (force) {
            return (List<Enterprise>) enterpriseRepository.findAll();
        }

        var user = userRepository.findByRole(Role.ROLE_SUPER_ADMIN);

        return enterpriseRepository.findAllByIdIsNot(user.getEnterprise().getId());
    }

    public Enterprise save(Enterprise enterprise) {
        return enterpriseRepository.save(enterprise);
    }

    @Transactional
    public void delete(Enterprise enterprise) {
        if (userRepository.findByRole(Role.ROLE_SUPER_ADMIN).getEnterprise().getId().equals(enterprise.getId())) {
            throw new IllegalArgumentException("Cannot delete the enterprise of the super admin");
        }

        enterpriseRepository.delete(enterprise);
    }

    public Enterprise findById(Integer id) {
        return enterpriseRepository.findById(id).orElse(null);
    }
}
