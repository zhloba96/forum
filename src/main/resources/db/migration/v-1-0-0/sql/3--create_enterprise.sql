create table if not exists enterprises
(
    id    serial not null primary key,
    name  text   not null,
    email text
);

insert into enterprises (name) values ('SoftServe');

alter table topics
    add column enterprise_id int not null default 1;

alter table users
    add column enterprise_id int not null default 1;

alter table topics
    add constraint fk_topics_enterprises
        foreign key (enterprise_id)
            references enterprises (id);

alter table users
    add constraint fk_users_enterprises
        foreign key (enterprise_id)
            references enterprises (id);