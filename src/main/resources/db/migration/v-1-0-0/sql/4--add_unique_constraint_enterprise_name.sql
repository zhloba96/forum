ALTER TABLE enterprises
    ADD CONSTRAINT uq_enterprises_name
        UNIQUE (name);